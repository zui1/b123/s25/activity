db.fruits.aggregate([

   		 {$match: {supplier: "Red Farms Inc"}},
   		 {$count:"itemsSupplied"},

		])

db.fruits.aggregate([

   		 {$match: {supplier: "Green Farming and Canning"}},
   		 {$count:"itemsSupplied"},

		])

db.fruits.aggregate([

   		 {$match: {onSale:true}},
   		 {$group: {_id:"$supplier", avgPricePerSupplier : {$avg: "$price"}}},

		])

db.fruits.aggregate([

   		 {$match: {onSale:true}},
   		 {$group: {_id:"$supplier", maxPricePerSupplier : {$max: "$price"}}},

		])

db.fruits.aggregate([

   		 {$match: {onSale:true}},
   		 {$group: {_id:"$supplier", minPricePerSupplier : {$min: "$price"}}},

		])